#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <sstream>

using namespace std;

double determinant(vector<vector<double>> mat, int n) {
    int D = 0;
    if (n == 1)
        return mat[0][0];
    vector<vector<double>> temp(n);
    for(int i = 0; i < n; i++){
        vector<double> arr(n);
        temp[i] = arr;
    }
    int sign = 1;
    for (int f = 0; f < n; f++) {
        int i = 0, j = 0;
        for (int row = 0; row < n; row++) {
            for (int col = 0; col < n; col++) {
                if (row != 0 && col != f) {
                    temp[i][j++] = mat[row][col];
                    if (j == n - 1) {
                        j = 0;
                        i++;
                    }
                }
            }
        }
        D += sign * mat[0][f]
             * determinant(temp, n - 1);
        sign = -sign;
    }
    return D;
}

void printMatrix(const vector<vector<double>>& mat){
    for(auto & r : mat){
        for(double s : r){
            printf("%10.2f ", s);
        }
        printf("\n");
    }
}

//projection of vector a on vector b
vector<double> proj(const vector<double> &a, const vector<double> &b){
    vector<double> result = b;
    double ab = 0;//dot product (a ,b)
    double bb = 0;//dot product (b ,b)
    for(int i = 0; i < a.size(); i++){
        ab += a[i] * b[i];
        bb += b[i] * b[i];
    }
    for(double & i : result){
        i *= ab/bb;
    }
    return result;
}

vector<double> vectorAMinusVectorB(const vector<double> &a, const vector<double> &b){
    vector<double> result = a;
    for(int i = 0; i < a.size(); i++){
        result[i] -= b[i];
    }
    return result;
}

//check if vectors orthogonal, vectors orthogonal if dot product is 0
bool checkOrthogonality(const vector<double> &a, const vector<double> &b){
    double dotProduct = 0;
    for(int i = 0; i < a.size(); i++){
        dotProduct += a[i] * b[i];
    }
    //-0.000000000001 < dotProduct < 0.000000000001 due to rounding errors
    return dotProduct < 0.000000000001 && dotProduct > -0.000000000001;
}

//if every vector is orthogonal on other vectors, then basis is orthogonal
bool checkBasisOrthogonality(const vector<vector<double>>& mat){
    for(int i = 0; i < mat.size(); i++){
        for(int ii = 0; ii < mat.size(); ii++){
            if(i != ii){
                if(!checkOrthogonality(mat[i], mat[ii])){
                    return false;
                }
            }
        }
    }
    return true;
}

//mat.size() = dimension of basis
vector<vector<double>> gramShmidt(const vector<vector<double>>& mat){
    vector<vector<double>> result = mat;
    for(int r = 0; r < mat.size(); r++){
        for(int p = 0; p < r; p++){
            result[r] = vectorAMinusVectorB(result[r], proj(result[r], result[p]));
        }
        //sqrt of dot product (x, x) to find orthonormal
        double xx = 0;
        for(double & s: result[r]){
            xx += s * s;
        }
        xx = sqrt(xx);
        for(int i = 0; i < result[r].size(); i++){
            result[r][i] *= xx;
        }
    }
    return result;
}

void help() {
    cout << "Description: "
         << endl << "\t This program can be used to orthogonalize basis from .txt file"
         << endl << "\t Throws error if vectors from file isn't basis or for incorrect input"
         << endl << "\t Creates new result file"
         << endl;
    cout << "Arguments:"
         << endl << "\t --help or -h to show the app manual"
         << endl;
    cout << "Example:" << endl << "./GSH [file]" << endl << endl;
    cout << endl << "nurmuedu@fel.cvut.cz" << endl << endl;
}

int main(int argc, char *argv[]) {

    if(argc == 1){
        cout  << "You have to write at least one file path." << endl;
        help();
        return 0;
    }
    if((string) argv[1] == "--help" || (string) argv[1] == "-h") {
        help();
        return 0;
    }

    if(argc > 2){
        help();
        return 0;
    }

    vector<vector<double>> basis = {};
    vector<vector<double>> basisOrt = {};
    ifstream in(argv[1]);
    string line;
    string number;
    while(!in.eof()) {
        vector<double> tmp = {};
        getline(in,line);

        stringstream strStream(line);
        while (std::getline(strStream, number, ','))
        {
            tmp.push_back(stod(number));
        }
        basis.push_back(tmp);
    }
    for(vector<double> & v : basis){
        if(v.size() != basis.size()){
            cout<<"There has to be N vectors, and vectors has to be N dimensional"<<endl;
            in.close();
            return 0;
        }
    }
    if(determinant(basis, basis.size()) == 0){
        cout<<"This set of vector isn't basis."<<endl;
        in.close();
        return 0;
    }

    basisOrt = gramShmidt(basis);
    string filename = ((string)argv[1]).substr(0, ((string)argv[1]).length()-4);
    filename += "RESULT.txt";
    ofstream outfile (filename);
    int index{1};
    for(auto & i : basisOrt){
        outfile << "(";
        for(int ii = 0; ii < i.size(); ii++){
            outfile << i[ii];
            if(ii != i.size() - 1){
                outfile << ", ";
            }
        }
        outfile << ") ";
        outfile << "= v" << index++;
        outfile << endl;
    }
    cout << (checkBasisOrthogonality(basisOrt) ? "OK" : "Not OK");
    outfile.close();
    return 0;
}
